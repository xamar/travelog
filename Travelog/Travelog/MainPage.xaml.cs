﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Travelog
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            var assembly = typeof(MainPage);
            noteIcon.Source = ImageSource.FromResource("Travelog.Assets.Images.note.png", assembly);
            
		}
        public void LogIn_Clicked(object sender, EventArgs e)
        {
            
            Regex pinPattern = new Regex("[0-9]{3}");
            Regex userNamePattern = new Regex("[a-z]{7}");
            if (userNamePattern.IsMatch(userNameEntry.Text) && pinPattern.IsMatch(pinEntry.Text))
            {
                Navigation.PushAsync(new HomePage());
            }
            else
            {
                DisplayAlert("Failure", "Login", "Ok");
            }
        }
    }
}