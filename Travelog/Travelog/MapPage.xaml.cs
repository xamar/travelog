﻿using Plugin.Geolocator;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelog.Model;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Travelog
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
        Plugin.Geolocator.Abstractions.IGeolocator locator;
		public MapPage ()
		{
			InitializeComponent ();
		}

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();

            var center = new Position(position.Latitude, position.Longitude);
            var mapSpan = new MapSpan(center, 1, 1);
            locationMap.MoveToRegion(mapSpan);


            locator.PositionChanged += Locator_PositionChanged;
            await locator.StartListeningAsync(TimeSpan.FromSeconds(0), 100);

            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Moment>();
                var moments = conn.Table<Moment>().ToList();
                DisplayInMap(moments);
            }
        }

        private void DisplayInMap(List<Moment> moments)
        {
            foreach (var moment in moments)
            {
                try
                {
                    var positon = new Position(moment.Latitude, moment.Longitude);
                    var pin = new Pin()
                    {
                        Type = PinType.SavedPin,
                        Position = positon,
                        Label = moment.Place,
                        Address = moment.Adress,
                    };
                    locationMap.Pins.Add(pin);
                }
                catch(NullReferenceException nre)
                {

                }
                catch (Exception ex)
                {

                }
            }
        }

        private void Locator_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {            
            var center = new Position(e.Position.Latitude, e.Position.Longitude);
            var mapSpan = new MapSpan(center, 1, 1);
            locationMap.MoveToRegion(mapSpan);
        }

        protected async override void OnDisappearing()
        {
            base.OnDisappearing();
            locator.PositionChanged -= Locator_PositionChanged;
            await locator.StopListeningAsync();
        }
    }
}