﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Travelog.Model
{
    class Moment
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Title { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public string Place { get; set; }

        public string Adress { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
