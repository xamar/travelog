﻿using Plugin.Geolocator;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelog.Model;
using Travelog.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelog
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewPostPage : ContentPage
	{
		public NewPostPage ()
		{
			InitializeComponent ();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();

            var venues = await VenueLogic.GetVenues(position.Latitude, position.Longitude);
            placesListView.ItemsSource = venues;
        }

        public void SaveMoment_Clicked(object sender, EventArgs e)
        {
            try
            {
                var selectedPlace = placesListView.SelectedItem as Venue;

                Moment moment = new Moment();
                moment.Title = titleEntry.Text;
                moment.Description = descriptionEntry.Text;
                moment.Place = selectedPlace.name;
                moment.Adress = selectedPlace.location.address;
                moment.Latitude = selectedPlace.location.lat;
                moment.Longitude = selectedPlace.location.lng;

                using (SQLiteConnection conn = new SQLiteConnection(databasePath: App.DatabaseLocation))
                {
                    conn.CreateTable<Moment>();
                    int rows = conn.Insert(moment);

                    if (rows > 0)
                        DisplayAlert("s", "u", "c");
                    else
                        DisplayAlert("f", "a", "i");
                }
                Navigation.PushAsync(new HomePage());
            }
            catch(NullReferenceException nre)
            {

            }
            catch(Exception ex)
            {
                
            }
        }
	}
}