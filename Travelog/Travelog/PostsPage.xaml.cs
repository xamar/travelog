﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelog.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelog
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PostsPage : ContentPage
	{
		public PostsPage ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Moment>();
                var moments = conn.Table<Moment>().ToList();
                postsListView.ItemsSource = moments;
            }           
            
        }
    }
}