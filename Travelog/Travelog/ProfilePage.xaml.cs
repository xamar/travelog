﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelog.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelog
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProfilePage : ContentPage
	{
		public ProfilePage ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            using(SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                try
                {
                    conn.CreateTable<Moment>();
                    var momentsTable = conn.Table<Moment>().ToList();

                    var places = (from m in momentsTable
                                  orderby m.Id
                                  select m.Place).Distinct().ToList();
                    Dictionary<string, int> placesCount = new Dictionary<string, int>();
                    foreach (var place in places)
                    {
                        if (place != null)
                        {
                            int count = (from moment in momentsTable
                                         where moment.Place == place
                                         select moment).ToList().Count;
                            placesCount.Add(place, count);
                        }
                    }
                    placesWithCountListView.ItemsSource = placesCount;
                    postsCountLabel.Text = momentsTable.Count.ToString();
                }
                catch(NullReferenceException nre)
                {

                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}